let modal;

function afficherModal(idModal){
  modal = document.getElementById(idModal);
  modal.style.display = "flex";
}

function closeModal(){
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

function testEchap(e){
  if(modal && (modal.style.display == "flex") && (e.keyCode == 27 || e.which == 27)){
    closeModal();
  }
}
