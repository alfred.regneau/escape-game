function positionnerContenu(){
  /**
  * Abaisse le main pour éviter que le header(ayant un position fixed) ne cache le haut de celui-ci.
  * Met une taille minimum au main qui prends la valeur de la hauteur de la fenetre - (celle du header + celle du footer)
  */
  document.getElementById("main").style.paddingTop += (document.getElementById('header').offsetHeight) + "px";

  var total = document.getElementById('header').offsetHeight + document.getElementById('footer').offsetHeight;
  document.getElementById('main').style.minHeight = (window.innerHeight - total) + "px";
}

// L'animation du header de la page lors d'un scroll
window.addEventListener('scroll', function scroll_menu(e){
  var taille = document.getElementById("header").offsetHeight;
  if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
    document.getElementById("header").style.height = "3em";
    document.getElementById("header-titre").style.fontSize = "1.2em";
  }else{
    document.getElementById("header").style.height = "5em";
    document.getElementById("header-titre").style.fontSize = "1.5em";
  }
});

/* MENUS : HEADER, ASIDE, FOOTER */
function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


function attribuerTheme(origine) {
  let themeDonne = origine.getAttribute("value");
  document.cookie="theme=" + themeDonne;
  miseAJourTheme();
}

function miseAJourTheme() {
  let theme = getCookie("theme");
  if (theme===""){
    changerFeuilleStyle("theme", "bleu");
  } else {
    changerFeuilleStyle("theme", theme);
  }
}

function changerFeuilleStyle(id, feuilleStyle){
  let head = document.getElementsByTagName("head")[0];
  let link = document.createElement("link");
  link.rel = "stylesheet";
  link.href = "/static/css/" + feuilleStyle + ".css"
  link.id = "theme";
  let existingLink = document.getElementById("theme");
  if (existingLink) {
     head.removeChild(document.getElementById("theme"));
  }
  head.appendChild(link);
}
/* FIN MENUS */

window.onresize = positionnerContenu;
