let input = document.getElementById("formulaire_profil");
input.addEventListener('change', updateImage);
input.addEventListener('change', updateInputValue);

function updateImage() {
	let inputNode = document.getElementById("fichier");
	let imageNode = document.getElementById("avatar_img");

	let regexInput = new RegExp('^input$', 'i');
	let regexImg = new RegExp('^img$', 'i');

	if (regexInput.test(inputNode.tagName) && regexImg.test(imageNode.tagName)) {
		let selectedFile = inputNode.files;
		if (selectedFile.length === 1) {
			console.log(window.URL.createObjectURL(selectedFile[0]));
	        imageNode.src = window.URL.createObjectURL(selectedFile[0]);
	        console.log();
	        console.log(imageNode.src);
		}
	}
}

function updateInputValue() {
	let fichier = document.getElementById('fichier').files[0];
  	
  	if(fichier.type.startsWith("image/")){
	    let reader = new FileReader();

	    reader.addEventListener('load', function(){
	      nameBackground = this.result;
	      document.getElementById('_avatar').value = this.result;
	    });

	    reader.readAsDataURL(fichier);
	    console.log(document.getElementById('_avatar').value);
  	}
}